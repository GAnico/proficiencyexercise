//
//  AppDelegate.h
//  ProficiencyExercise
//
//  Created by nico.noel.g.solis on 2/23/15.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

