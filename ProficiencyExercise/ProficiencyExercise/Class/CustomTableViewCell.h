//
//  CustomTableViewCell.h
//  ProficiencyExercise
//
//  Created by nico.noel.g.solis on 2/23/15.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FONT_SIZE 12.0f
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 20.0f
#define CELL_DEFAULT_HEIGHT 50.0f
#define CELL_TITLE_HEIGHT 30
#define CELL_IMG_WIDTH 65

@interface CustomTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel * lblTitle;
@property (nonatomic, strong) UILabel * lblDesc;
@property (nonatomic, strong) UIImageView * ivThumbnail;

@end
