//
//  CustomTableViewCell.m
//  ProficiencyExercise
//
//  Created by nico.noel.g.solis on 2/23/15.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// add object for customized cell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        lblTitle = [[UILabel alloc] init];
        [lblTitle setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
        [lblTitle setTextColor:[UIColor blueColor]];
        [lblDesc setNumberOfLines:0];
        [self addSubview:lblTitle];
        
        lblDesc = [[UILabel alloc] init];
        [lblDesc setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
        [lblDesc setTextColor:[UIColor blackColor]];
        [lblDesc setNumberOfLines:0];
        [self addSubview:lblDesc];
        
        ivThumbnail = [[UIImageView alloc] initWithFrame:CGRectMake([self bounds].size.width - 70, CELL_TITLE_HEIGHT + 5, CELL_IMG_WIDTH, 65)];
        [self addSubview:ivThumbnail];
    }
    return self;
}

@synthesize lblTitle;
@synthesize lblDesc;
@synthesize ivThumbnail;

@end
