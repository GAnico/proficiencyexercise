//
//  MainViewController.h
//  ProficiencyExercise
//
//  Created by nico.noel.g.solis on 2/23/15.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray * marrRowContent;
    UIRefreshControl * refreshControl;
    UILabel * lblMsg;
}

@property (nonatomic, strong) IBOutlet UITableView * tvRowList;

@end

