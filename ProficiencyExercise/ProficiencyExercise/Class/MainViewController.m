//
//  MainViewController.m
//  ProficiencyExercise
//
//  Created by nico.noel.g.solis on 2/23/15.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import "MainViewController.h"
#import "CustomTableViewCell.h"

#define kURL @"https://dl.dropboxusercontent.com/u/746330/facts.json"

#define kRow @"rows"
#define kTitle @"title"
#define kDesc @"description"
#define kImgURL @"imageHref"

static NSString * cellIdentifier = @"CustomCell";

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// setup for initial launching
- (void)initSetup {
    refreshControl = [[[UIRefreshControl alloc] init] autorelease];
    [refreshControl addTarget:self action:@selector(fetchData) forControlEvents:UIControlEventValueChanged];
    [refreshControl setBackgroundColor:[UIColor lightGrayColor]];
    [refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Fetching Data..."]];
    
    lblMsg = [[[UILabel alloc] init] autorelease];
    [lblMsg setFrame:CGRectMake(0, 0, [[self view] bounds].size.width, [[self view] bounds].size.height/2)];
    [lblMsg setText:@"No data is currently available. Please pull down to refresh."];
    [lblMsg setTextColor:[UIColor blackColor]];
    [lblMsg setNumberOfLines:0];
    [lblMsg setTextAlignment:NSTextAlignmentCenter];
    [lblMsg setFont:[UIFont fontWithName:@"Palatino-Italic" size:20]];
    [lblMsg sizeToFit];
    
    [tvRowList setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tvRowList setBackgroundView:lblMsg];
    
    marrRowContent = [[[[NSMutableArray alloc] init] retain] autorelease];
    [tvRowList setDelegate:self];
    [tvRowList setDataSource:self];
    [tvRowList addSubview:refreshControl];
}

#pragma mark - Table View Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [marrRowContent count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell * cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary * dict = [[NSDictionary alloc] initWithDictionary:[marrRowContent objectAtIndex:[indexPath row]]];
    [[cell lblTitle] setText:@""];
    if ([dict objectForKey:kTitle] != [NSNull null]) {
        [[cell lblTitle] setFrame:CGRectMake(5, 5, CELL_CONTENT_WIDTH, CELL_TITLE_HEIGHT)];
        [[cell lblTitle] setText:[dict objectForKey:kTitle]];
        [[cell lblTitle] sizeToFit];
    }
    
    [[cell lblDesc] setText:@""];
    if ([dict objectForKey:kDesc] != [NSNull null]) {
        CGSize dynamicSize = [[dict objectForKey:kDesc] sizeWithFont:[UIFont fontWithName:@"Arial" size:12.0f] constrainedToSize:CGSizeMake(200, 20000)];
        [[cell lblDesc] setFrame:CGRectMake(5, CELL_TITLE_HEIGHT + 5, CELL_CONTENT_WIDTH - (CELL_IMG_WIDTH + 20), dynamicSize.height)];
        [[cell lblDesc] setText:[dict objectForKey:kDesc]];
        [[cell lblDesc] sizeToFit];
    }
    
    [[cell ivThumbnail] setImage:nil];
    if ([dict objectForKey:kImgURL] != [NSNull null]) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[dict objectForKey:kImgURL]]];
            UIImage * image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{ [[cell ivThumbnail] setImage:image]; });
        });
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[marrRowContent objectAtIndex:[indexPath row]] objectForKey:kDesc] == [NSNull null]) {
        return CELL_DEFAULT_HEIGHT;
    }
    
    NSString * text = [[marrRowContent objectAtIndex:[indexPath row]] objectForKey:kDesc];
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint];
    CGFloat height = MAX(size.height, CELL_DEFAULT_HEIGHT);
    
    return height + (CELL_CONTENT_MARGIN * 2) + CELL_TITLE_HEIGHT;
}

#pragma mark Private Methods

- (void)fetchData {
    [self requestWithURL:kURL];
}

// Method for fetching the JSON data from a URL
- (void)requestWithURL:(NSString *)url {
    NSMutableURLRequest * request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse * requestResponse;
    NSData * requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    // Response
    NSString * responseString = [[[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding] autorelease];
    NSData * responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError * error;
    NSDictionary * responseJSON = [NSJSONSerialization JSONObjectWithData:responseData
                                                                  options:kNilOptions
                                                                    error:&error];
    for (NSDictionary * object in [responseJSON objectForKey:kRow]) {
        [marrRowContent addObject:object]; //NSLog(@"%@",object);
    }
    
    if ([marrRowContent count] > 0) {
        [[self navigationItem] setTitle:[responseJSON valueForKey:kTitle]];
        [tvRowList setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [tvRowList setBackgroundView:nil];
    }
    
    [tvRowList reloadData];
    [refreshControl endRefreshing];
}

@synthesize tvRowList;

@end
